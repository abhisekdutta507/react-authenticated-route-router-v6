const cacheApp = 'Authenticated Route';

this.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(cacheApp).then(async (cache) => {
      const manifest = await fetch('asset-manifest.json').then(r => r.json());
      const safeCacheURLs = [
        // google fonts to be cached
        'https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500&display=swap',

        // apis must be cached
        'https://source.unsplash.com/random',

        // chunk js, css files to be cached. get the links by inspecting the HTML template
        ...Object.values(manifest.files),

        // manifest
        '/manifest.json',

        // icons
        '/favicon.ico',
        '/logo192.png',
        '/logo512.png'
      ];
      const unsafeCacheURLs = [
        // routes to be cached
        '/course',
        '/login',
        '/'
      ];

      try {
        await cache.addAll(safeCacheURLs);
        // await cache.addAll(unsafeCacheURLs);

        const unsafePromises = unsafeCacheURLs.map((url) => cache.add(url));
        await Promise.all(unsafePromises);
      } catch(e) {
        console.log("@developers - cache error", e);
      }
    })
  );
});

this.addEventListener('fetch', (event) => {
  /**
   * @description when offline get the contents from cache
   */
  if (!navigator.onLine) {
    event.respondWith(
      caches.match(event.request).then((response) => {
        if (response) {
          return response;
        }
      })
    );
  }
});
