import { createContext, useState, useEffect } from 'react';
import { useMediaQuery } from '@mui/material';
import App from './App';
import * as localStorage from './localStorage';

export const AppContext = createContext({
    mode: undefined,
    setMode: () => {},
    name: '',
    loggedInData: undefined,
    setLoggedInData: () => {},
    isHeaderVisible: true,
    toggleIsHeaderVisible: () => {}
});

const AppController = ({ ...rest }) => {
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
    const name = 'Authenticated Route';
    const [loggedInData, setLoggedInData] = useState({
        username: localStorage.username
    });
    const [isHeaderVisible, toggleIsHeaderVisible] = useState(true);
    const [mode, setMode] = useState('dark');

    useEffect(() => {
        setMode(prefersDarkMode ? 'dark' : 'light');
    }, [prefersDarkMode]);

    return (
        <AppContext.Provider value={{
            mode,
            setMode,
            name,
            loggedInData,
            setLoggedInData,
            isHeaderVisible,
            toggleIsHeaderVisible,
            ...rest
        }}>
            <App />
        </AppContext.Provider>
    );
};

export default AppController;
