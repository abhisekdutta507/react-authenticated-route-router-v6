import { memo } from 'react';
import { Typography, Link } from '@mui/material';

const Copyright = memo(() => {
    const fullYear = new Date().getFullYear();

    return (
      <Typography variant="body2" color="text.secondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://mui.com/">
          React Authenticated Route
        </Link>{' '}
        {fullYear}
        {'.'}
      </Typography>
    );
});

export default Copyright;
