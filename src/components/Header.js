import { useContext, useState, memo, useMemo } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, AppBar, Toolbar, Typography, Button, IconButton, Menu, MenuItem } from '@mui/material';
import { Book, AccountCircle, Brightness2, WbSunny } from '@mui/icons-material';
import { AppContext } from '../App.controller';
import './Header.css';

const Header = memo(() => {
    const [anchorEl, setAnchorEl] = useState(null);
    const App = useContext(AppContext);
    const navigate = useNavigate();

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        App.setLoggedInData(undefined);
        localStorage.removeItem('username');
        navigate('/');
    };

    const handleThemeChange = () => {
        App.setMode(
            App.mode === 'dark'
                ? 'light'
                : 'dark'
        );
    };

    const appBarComponent = useMemo(() => {
        return (
            <>
                <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }} onClick={() => navigate('/')}>
                    {
                        App.loggedInData?.username
                            ? `Hi! ${App.loggedInData.username}`
                            : `${App.name}`
                    }
                </Typography>
                {
                    App.loggedInData?.username ? (
                        <div>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleMenu}
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                <MenuItem onClick={() => { navigate('/course'); setAnchorEl(null); }}>Course</MenuItem>
                                <MenuItem onClick={handleLogout}>Logout</MenuItem>
                            </Menu>
                        </div>
                    ) : <Button color="inherit" onClick={() => { navigate('/login'); setAnchorEl(null); }}>Login</Button>
                }
            </>
        );
    // eslint-disable-next-line
    }, [App.loggedInData?.username, anchorEl]);

    const appThemeMode = useMemo(() => {
        return (
            <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleThemeChange}
                color="inherit"
            >
                {
                    App.mode === 'dark'
                        ? <Brightness2 />
                        : <WbSunny />
                }
            </IconButton>
        );
    // eslint-disable-next-line
    }, [App.mode]);

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="relative">
                <Toolbar>
                    <Book sx={{ mr: 2 }} />
                    {appBarComponent}
                    {appThemeMode}
                </Toolbar>
            </AppBar>
        </Box>
    );
});

export default Header;
