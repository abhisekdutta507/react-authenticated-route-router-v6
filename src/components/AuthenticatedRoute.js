import { useContext, memo } from 'react';
import { Navigate } from 'react-router-dom';
import { AppContext } from '../App.controller';

const AuthenticatedRoute = memo(({ valid = true, component }) => {
    const App = useContext(AppContext);

    return valid
        ? App.loggedInData?.username
        ? component
        : <Navigate to="/login" />
        : !App.loggedInData?.username
        ? component
        : <Navigate to="/" />;
});

export default AuthenticatedRoute;
