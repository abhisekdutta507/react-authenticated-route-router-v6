import { memo } from 'react';

const SuspenseFallback = memo(() => {
    return (
        <>Loading...</>
    );
});

export default SuspenseFallback;
