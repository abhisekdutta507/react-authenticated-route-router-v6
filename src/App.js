import { useContext, useMemo } from 'react';
import { Routes, Route } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import { AppContext } from './App.controller';
import { HomeController, LoginController, CourseController } from './pages';
import { Header, AuthenticatedRoute } from './components';
import './App.css';

const App = () => {
  const App = useContext(AppContext);

  const theme = useMemo(() => createTheme({
    palette: {
      mode: App.mode,
    },
  }), [App.mode]);

  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {
          App.isHeaderVisible
            ? <Header />
            : null
        }
        <Routes>
          <Route path="/" element={<HomeController />} />
          <Route path="login" element={<AuthenticatedRoute valid={false} component={<LoginController />} />} />
          <Route path="course" element={<AuthenticatedRoute component={<CourseController />} />} />
        </Routes>
      </ThemeProvider>
    </div>
  );
}

export default App;
