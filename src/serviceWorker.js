/**
 * @description create the service worker using native API
 * @tutorial https://youtu.be/PPbzwh5vdIA
 */
function serviceWorker() {
    const swURL = `${process.env.PUBLIC_URL}/sw.js`;
    navigator.serviceWorker.register(swURL).then((response) => {});
}
  
export default serviceWorker;
