import { createContext, useState, Suspense, lazy } from 'react';
import { SuspenseFallback } from '../components';

const Login = lazy(() => import('./Login'));

export const LoginContext = createContext({
    login: undefined,
    setLogin: () => {}
});

const LoginController = ({ ...rest }) => {
    const [login, setLogin] = useState(undefined);

    return (
        <LoginContext.Provider value={{
            login,
            setLogin,
            ...rest
        }}>
            <Suspense fallback={<SuspenseFallback />}>
                <Login />
            </Suspense>
        </LoginContext.Provider>
    );
};

export default LoginController;
