import { createContext, useState, Suspense, lazy } from 'react';
import { SuspenseFallback } from '../components';

const Home = lazy(() => import('./Home'));

export const HomeContext = createContext({
    name: undefined,
    setName: () => {},
    mainFeaturedPost: undefined,
    featuredPosts: []
});

const HomeController = ({ ...rest }) => {
    const mainFeaturedPost = {
        title: 'Title of a longer featured blog post',
        description:
          "Multiple lines of text that form the lede, informing new readers quickly and efficiently about what's most interesting in this post's contents.",
        image: 'https://source.unsplash.com/random',
        imageText: 'main image description',
        linkText: 'Continue reading…',
    };
    const featuredPosts = [
        {
          title: 'Featured post',
          date: 'Nov 12',
          description:
            'This is a wider card with supporting text below as a natural lead-in to additional content.',
          image: 'https://source.unsplash.com/random',
          imageLabel: 'Image Text',
        },
        {
          title: 'Post title',
          date: 'Nov 11',
          description:
            'This is a wider card with supporting text below as a natural lead-in to additional content.',
          image: 'https://source.unsplash.com/random',
          imageLabel: 'Image Text',
        },
    ];
    const [name, setName] = useState('Home');

    return (
        <HomeContext.Provider value={{
            name,
            setName,
            mainFeaturedPost,
            featuredPosts,
            ...rest
        }}>
            <Suspense fallback={<SuspenseFallback />}>
                <Home />
            </Suspense>
        </HomeContext.Provider>
    );
};

export default HomeController;
