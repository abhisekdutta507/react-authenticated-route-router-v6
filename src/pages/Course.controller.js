import { createContext, useState, Suspense, lazy } from 'react';
import { SuspenseFallback } from '../components';

const Course = lazy(() => import('./Course'));

export const CourseContext = createContext({
    course: undefined,
    setCourse: () => {}
});

const CourseController = ({ ...rest }) => {
    const [course, setCourse] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9]);

    return (
        <CourseContext.Provider value={{
            course,
            setCourse,
            ...rest
        }}>
            <Suspense fallback={<SuspenseFallback />}>
                <Course />
            </Suspense>
        </CourseContext.Provider>
    );
};

export default CourseController;
