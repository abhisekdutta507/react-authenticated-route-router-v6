import HomeController, { HomeContext } from './Home.controller';
import LoginController, { LoginContext } from './Login.controller';
import CourseController, { CourseContext } from './Course.controller';

export {
    HomeContext,
    LoginContext,
    CourseContext,

    HomeController,
    LoginController,
    CourseController
};
