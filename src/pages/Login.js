import { useContext, useRef, memo, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { Avatar, Button, Typography, TextField, Box, Grid, Paper } from '@mui/material';
import { LockOutlined } from '@mui/icons-material';
import { AppContext } from "../App.controller";
import { Copyright } from '../components';

const Image = memo(() => {
    return (
        <Grid
            item
            xs={false}
            sm={4}
            md={7}
            sx={{
                backgroundImage: 'url(https://source.unsplash.com/random)',
                backgroundRepeat: 'no-repeat',
                backgroundColor: (t) =>
                t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                backgroundSize: 'cover',
                backgroundPosition: 'center',
            }}
        />
    );
});

const Login = () => {
    const App = useContext(AppContext);
    const form = useRef();
    const navigate = useNavigate();

    useEffect(() => {
        App.toggleIsHeaderVisible(false);

        return () => {
            App.toggleIsHeaderVisible(true);
        };
    // eslint-disable-next-line
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();
        const username = form.current.username.value;

        if (!username) {
            return;
        }

        App.setLoggedInData({
            username
        });
        localStorage.setItem('username', username);
        navigate('/course');
    };

    return (
        <div className="Login">
            <Grid container component="main" sx={{ height: '100vh' }}>
                <Image />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <Box
                        sx={{
                        my: 8,
                        mx: 4,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                            <LockOutlined />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Login
                        </Typography>
                        <Box ref={form} component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1, width: "100%" }}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="username"
                                label="Username"
                                name="username"
                                autoFocus
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 2, mb: 2 }}
                            >
                                Login
                            </Button>
                            <Copyright />
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </div>
    );
};

export default Login;
