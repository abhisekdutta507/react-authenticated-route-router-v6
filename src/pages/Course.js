import { memo, useContext } from "react";
import { Card, CardMedia, CardContent, CardActions, Button, Container, Stack, Typography, Box, Grid } from '@mui/material';
import { CourseContext } from ".";
import { Footer } from '../components';

const CourseCard = memo(() => {
    return (
        <Card
            sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
        >
            <CardMedia
                component="img"
                image="https://source.unsplash.com/random"
                alt="random"
            />
            <CardContent sx={{ flexGrow: 1 }}>
                <Typography gutterBottom variant="h5" component="h2">
                    Heading
                </Typography>
                <Typography>
                    This is a media card. You can use this section to describe the
                    content.
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">View</Button>
                <Button size="small">Edit</Button>
            </CardActions>
        </Card>
    );
});

const CourseLayout = memo(() => {
    return (
        <Box
            sx={{
                bgcolor: 'background.paper',
                pt: 8,
                pb: 6,
            }}
        >
            <Container maxWidth="sm">
                <Typography
                    component="h1"
                    variant="h2"
                    align="center"
                    color="text.primary"
                    gutterBottom
                >
                    Course layout
                </Typography>
                <Typography variant="h5" align="center" color="text.secondary" paragraph>
                    Something short and leading about the collection below—its contents,
                    the creator, etc. Make it short and sweet, but not too short so folks
                    don&apos;t simply skip over it entirely.
                </Typography>
                <Stack
                    sx={{ pt: 4 }}
                    direction="row"
                    spacing={2}
                    justifyContent="center"
                >
                    <Button variant="contained">Main call to action</Button>
                    <Button variant="outlined">Secondary action</Button>
                </Stack>
            </Container>
        </Box>
    );
});

const CourseList = memo(() => {
    const Course = useContext(CourseContext);

    return (
        <Container sx={{ py: 8 }} maxWidth="md">
            {/* End hero unit */}
            <Grid container spacing={4}>
                {
                    Course.course.map((card) => (
                        <Grid item key={card} xs={12} sm={6} md={4}><CourseCard /></Grid>
                    ))
                }
            </Grid>
        </Container>
    );
});

const Course = () => {
    return (
        <div className="Course">
            <main>
                {/* Hero unit */}
                <CourseLayout />
                <CourseList />
            </main>
            {/* Footer */}
            <Footer />
            {/* End footer */}
        </div>
    );
};

export default Course;
